Clifford Dunn
OSU CS 372
Project 1


Files in zip:
README.md, chatserve.py, chatclient.c makefile

First, simply run: make

Running the chat server:
Runs with Python2: ./chatserve.py [portNumber] [serverNick]
note: portNumber and serverNick are optional. However, if port is in use, the command will fail.

Running the chat client:
Runs after compiled with gcc: ./chatclient {hostname} {portNumber}

This has been tested on OSU flip3
To duplicate my results:
./chatserve.py 7432

On separate window:
./chatclient localhost 7432

EXTRA CREDIT: Text output for server and client is colorful!
EXTRA CREDIT: The chat server also creates a log of the chat! 
