/*******************************************************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS372 - Project #1
 * File Name: chatclient.c
 * Language: C
 *******************************************************************************************************************/

// Include Libraries
#include <netdb.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// SIZE macros. Can be changed if necessary during compile time
#ifndef MAXHANDLESIZE
#define MAXHANDLESIZE 11
#endif

#ifndef MAXMSGSIZE
#define MAXMSGSIZE 501
#endif

// EC: Colored Text Output
#define BLUE "\x1b[36m"
#define GREEN "\x1b[32m"
#define YELLOW "\x1b[33m"
#define RED "\x1b[31m"
#define PINK "\x1b[35m"
#define ENDC "\x1b[0m"


// Error function used for reporting issues Thanks to professor Brewster
void error(const char *msg) {
  perror(msg);
  exit(1);
}

/*
 * Function: SetHandle
 * Input: The memory allocated for the handle
 * Output: The handle has been written to memory
 */
void SetHandle(char handle[]) {
  // Query the user for the handle. Place it in the allocated memory.
  fprintf(stdout, PINK  "What is your handle? " ENDC);
  fgets(handle, 10, stdin);
  // Remove the trailing newline character
  handle = strtok(handle, "\n");
  fprintf(stdout, PINK "Hello, %s!\n" ENDC, handle);
}

/*
 * Function: RecvHandle
 * Input: The memory allocated for the server's handle
 * Output: The server's handle has been written to memory
 */
void RecvHandle(char servHandle[], int socket) {
  // Read the data directly from the socket
  read(socket, servHandle, MAXHANDLESIZE);
}

/*
 * Function: SendHandle
 * Input: The memory for the client's handle, the socket number to read/write
 * Output: The client's handle has been sent to the server
 */
void SendHandle(char handle[], int socket) {
  // Write the data directly to the socket
  write(socket, handle, MAXHANDLESIZE);
}

/*
 * Function: ValidateConnection
 * Input: The memory for the client's handle,
 *        The memory for the server's handle,
 *        The socket number to read/write
 * Output: Handles have been exchanged
 */
void ValidateConnection(char handle[], char servHandle[], int socket) {
  // Activate the functions to send and receive the handles
  SendHandle(handle, socket);
  RecvHandle(servHandle, socket);
}

/*
 * Function: Chat
 * Input: The memory for the client's handle,
 *        The memory for the server's handle,
 *        The socket number to read/write
 * Output: The chat between client and server is initiated
 */
void Chat(char handle[], char servHandle[], int socket) {
  // Create the variables to hold messages
  char msgSend[MAXMSGSIZE];
  char msgRecv[MAXMSGSIZE];

  // Loop will continue until the chat is ended by client or server.
  while (1) {
    // Out of habit, I fflush stdin and stdout
    fflush(stdin);
    fflush(stdout);
    // Makesure the memory is zeroed out.
    memset(msgSend, '\0', sizeof(msgSend));
    memset(msgRecv, '\0', sizeof(msgRecv));
    // To receive input, display client's handle and wait for input
    fprintf(stdout, GREEN "%s> " ENDC, handle);
    fgets(msgSend, 500, stdin);
    // For loop to remove the newline character.
    int i;
    for (i = 0; i < MAXMSGSIZE; i++) {
      if (msgSend[i] == '\n') {
        msgSend[i] = '\0';
        break;
      }
    }

    // If the client enters '\quit', then the chat is ended
    if (strcmp(msgSend, "\\quit") == 0) {
      close(socket);
      fprintf(stdout, YELLOW "Now leaving chat!\n" ENDC);
      break;
    }

    // Send the message to the server
    write(socket, msgSend, MAXMSGSIZE);
    // Wait and receive a message from the server
    read(socket, msgRecv, MAXMSGSIZE);
    // If the server has quit, the chat must end.
    if (strcmp(msgRecv, "\\quit") == 0) {
      fprintf(stdout, YELLOW "%s has closed the connection!\n" ENDC, servHandle);
      fprintf(stdout, YELLOW "Now leaving chat!\n" YELLOW);
      break;
    }
    // Otherwise, print what the server sent
    fprintf(stdout, BLUE "%s> %s\n" ENDC, servHandle, msgRecv);
  }
}

/******************************************************************************************************************
 * Function: main
 * Description: This is the server program that sends all data to the
 *decryption network client
 ********************************************************************************************************************/
int main(int argc, char *argv[]) {
  /*******************************************************************
   Some thanks for the socket programming goes to Beej's guide.
   However, most information was learned during my final project for
   OSU CS 344 with Professor Benjamin Brewster
  *******************************************************************/

  if (argc < 3) {
    error("Usage: ./chatclient {hostname}{portNum}\n");
    exit(1);
  }
  // Create the socket file descriptor identity and declare necessary
  // variables

  int socketFD, portNum;
  struct sockaddr_in serverAddr;
  struct hostent *serverHostInfo;

  // Allocate the required memory for the handles
  char handle[MAXHANDLESIZE];
  char servHandle[MAXHANDLESIZE];

  // Zero out the handles
  memset(handle, '\0', sizeof(handle));
  memset(servHandle, '\0', sizeof(handle));

  // Get the name of the client's handle
  SetHandle(handle);

  // Allocate the proper server and port based on the commandline input
  memset((char *)&serverAddr, '\0', sizeof(serverAddr));
  serverHostInfo = gethostbyname(argv[1]);
  portNum = atoi(argv[2]);
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(portNum);
  if (serverHostInfo == NULL) {
    error("CLIENT: ERROR, no such host\n");
    exit(2);
  }
  memcpy((char *)&serverAddr.sin_addr.s_addr, (char *)serverHostInfo->h_addr,
         serverHostInfo->h_length); // Copy in the address

  // Create the socket with which to connect.
  socketFD = socket(AF_INET, SOCK_STREAM, 0);
  if (socketFD < 0)
    error("CLIENT: ERROR opening socket");

  // Connect to the socket.
  if (connect(socketFD, (struct sockaddr *)&serverAddr, sizeof(serverAddr)) < 0)
    error("CLIENT: ERROR connecting");

  // Perform the validation with the server.
  ValidateConnection(handle, servHandle, socketFD);

  // Alert the user that the server is connected and waiting.
  fprintf(stdout, PINK "Connecting to chat with %s\n" ENDC, servHandle);

  // Perform the chat.
  Chat(handle, servHandle, socketFD);

  return 0;
}
