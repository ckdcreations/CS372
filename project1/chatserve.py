#!/bin/python2


# -------------------------------------------------------------------
# Name: Clifford Dunn
# Description: OSU CS372 - Project #1
# File Name: chatserve.py
# Language: Python 2
# -------------------------------------------------------------------

# Important libraries used
import sys
import socket

# -------------------------------------------------------------------
# Class: txtColors 
# Input: None
# Output: Outputs text to command line in various colors
# -------------------------------------------------------------------
class txtColors:
        BLUE = '\033[94m'
        GREEN = '\033[92m'
        YELLOW = '\033[93m'
        RED = '\033[91m'
        PINK = '\033[95m'
        ENDC = '\033[0m'

# -------------------------------------------------------------------
# Function: validateConnection
# Input: The client's socket, the server name
# Output: Returns the client's name
# -------------------------------------------------------------------
def validateConnection(clientSock, serverNick):
    # Receive the client's name
    clientNick = clientSock.recv(11)[0:-1]
    # Send the server's name
    clientSock.send(serverNick)
    # Make sure we keep the client's name
    return clientNick


# -------------------------------------------------------------------
# Function: chat
# Input: The client's socket, the client's name, the server name
# Output: Performs the chat.
# -------------------------------------------------------------------
def chat(clientSock, clientNick, serverNick, chatLog):
    # endless loop until something external stops it

    connClosedClient = "Connection closed by %s" %(clientNick)
    connClosedServer = "Connection closed by %s" %(serverNick)
    connWait = "Waiting for new connection"
    while 1:
        # Receive a message from the client
        msgRecv = clientSock.recv(501)[0:-1]

        # If the incoming message is nothing, then the client
        # has left the chat. Close this function and wait for a new chat.
        if msgRecv == "":
            print(txtColors.YELLOW + connClosedClient)
            chatLog.write(connClosedClient + "\n")
            print(txtColors.PINK + connWait)
            chatLog.write(connWait + "\n")
            break

        # Output the chat from the client. Include the known client's name
        print(txtColors.BLUE + "%s> %s "  %(clientNick, msgRecv))
        chatLog.write("%s> %s \n" %(clientNick, msgRecv))

        # Prepare the msgSend variable to hold a string
        msgSend= ""
        # Make sure that an acceptable number of characters is sent.
        while len(msgSend) == 0 or len(msgSend) > 500:
            # Read from the keyboard
            msgSend = raw_input(txtColors.GREEN + "%s> " %(serverNick))
            chatLog.write("%s> %s\n" %(serverNick, msgSend))

        # When the server wants to disconnect, this will disconnect
        # the client's chat. It will then wait for a new connection
        if msgSend == "\quit":
            clientSock.send(msgSend)
            print(txtColors.YELLOW + connClosedServer)
            chatLog.write(connClosedServer + "\n")
            print(txtColors.PINK + connWait)
            chatLog.write(connWait + "\n")
            break

        clientSock.send(msgSend)



# -------------------------------------------------------------------
# Function: main
# Input: No input to main. However, the option exists at the call
#        to the program to include the portname and server name.
# Output: Runs the program on the server
# -------------------------------------------------------------------
if __name__ == "__main__":
    # This is the default serverNick
    serverNick = "chatServer"

    # EC: Server text is output in various colors
    # EC: Server creates a chat log!
    # Open the file for chat log. All printed arguments will be written to this file
    chatLog = open("ServerLog.txt", "w")
    # If there are less than two arguments at input, we will use
    # the default port number. In this case, that is 5555
    if int(len(sys.argv)) < 2:
        noPort = "No port specified. Connecting to default"
        print(noPort)
        chatLog.write(noPort + "\n")
        port = 55555
    # Otherwise, if there are two inputs, the second is the port
    elif int(len(sys.argv)) == 2:
        port = int(sys.argv[1])
    # Otherwise, if there are three inputs,
    # the second is the port, and the third is the serverNick
    elif int(len(sys.argv)) == 3:
        custNick = "Custom Nick Detected!"
        print(custNick)
        chatLog.write(custNick + "\n")
        serverNick = str(sys.argv[2])
        port = int(sys.argv[1])
    # If there are too many arguments, we abort.
    elif int(len(sys.argv)) > 3:
        manyArgs = "Too many arguments. Aborting."
        print(manyArgs + "\n")
        chatLog.write(manyArgs + "\n")
        exit(1)

    # Insure that we are on an acceptable port for communication
    if port >= 1024 and port <= 65535:
        usePort = "Using Port: " + str(port)
        servNick = "serverNick: " + str(serverNick)
        print (usePort)
        chatLog.write(usePort + "\n")
        print (servNick)
        chatLog.write(servNick + "\n")
    # If the desired port is out of range, use the default.
    else:
        ooRange = "Selected port is out of range. Connecting to default"
        print(ooRange)
        chatLog.write(ooRange + "\n")
        port = 55555
        usePort = "Using Port: " + str(port)
        servNick = "serverNick: " + str(serverNick)
        print (usePort)
        chatLog.write(usePort + "\n")
        print (servNick)
        chatLog.write(servNick + "\n")

    # Connection data. Connect to the localhost machine and the selected port.
    host = 'localhost'
    addr = (host, port)

    # Perform the socket connections. Extra special thanks to
    # https://docs.python.org/2.7/library/socket.html
    # Originally intended to be implemented with Python 3, but there were
    # Many unusual issues that disappeared with the use of Python 2.
    serverSock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    serverSock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    serverSock.bind(addr)
    serverSock.listen(1)

    chatLog.close()
    # The Actual chat loop.
    # This will loop endlessly until a Ctrl+C
    while 1:
        chatLog = open("ServerLog.txt", "a")
        # Waits for connection to a chat client
        clientSock, clientAddr = serverSock.accept()
        newClient = "New connection from: %s" %(str(clientAddr))
        print (txtColors.PINK + newClient)
        chatLog.write(newClient + "\n")

        # Perform the chat function
        chat(clientSock, validateConnection(clientSock, serverNick), serverNick, chatLog)
        chatLog.write("\n\n\n")
        chatLog.close()
        clientSock.close()
        print(txtColors.ENDC)
