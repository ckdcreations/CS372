#!/bin/python3

# ----------------------------------------------------------------------------
# Name: Clifford Dunn
# Description: OSU CS372 - Project #2
# File Name: ftclient.py
# Language: Python 3
# ----------------------------------------------------------------------------

# Important Libraries
import socket
import sys
import os
from time import sleep
import struct
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL)


VALID_YES = ["", "Y", "y"]


# ----------------------------------------------------------------------------
# Function: properOutput
# Input: None
# Output: Displays the proper use of the command line syntax
# ----------------------------------------------------------------------------
def properOutput():
    print("Guidelines for ftclient.py:")
    print("This program expects either five or six arguments")
    print("Example 1: This command will list the directory")
    print("./ftclient.py <serverHost> <portNum> -l <dataPortNum>")
    print("Example 2: This command will request a file from the directory")
    print("./ftclient.py <serverHost> <portNum> -g  <fileName> <dataPortNum>")
    print("NOTE: If connecting to an OSU server, simply use flip1, flip2, or flip3 for <serverHost>")

# ----------------------------------------------------------------------------
# Function: parseCommand
# Input: From command line: serverHost, serverPortNum, command, fileName,
#        dataPortNum
# Output: Verifies each argument to make sure the program is run correctly.
# ----------------------------------------------------------------------------
def parseCommand():
    global serverHost
    global serverPortNum
    global command
    global fileName
    global dataPortNum

    if (sys.argv[1] == "flip1") or (sys.argv[1] == "flip2") or (sys.argv[1] == "flip3"):
        serverHost = sys.argv[1] + ".engr.oregonstate.edu"
    else:
        serverHost = sys.argv[1]

    serverPortNum = int(sys.argv[2])
    if serverPortNum not in range(1024, 65536):
        print("ERROR: Server port number out of range. Please select a port number between 1024 and 65535")
        exit(1)

    command = sys.argv[3]

    if command == "-g":
        fileName = sys.argv[4]
    else:
        fileName = None


    # [-1] takes the last argument in the array
    dataPortNum = int(sys.argv[-1])
    if dataPortNum not in range(1024, 65536):
        print("ERROR: Data port number out of range. Please select a port number between 1024 and 65535")
        exit(1)

    if serverPortNum == dataPortNum:
        print("ERROR: Can not use the same port for server and data")
        exit(1)


# ----------------------------------------------------------------------------
# Function: connectionInfo
# Input: None
# Output: Prints the information that is being used to connect and command
#         the server
# ----------------------------------------------------------------------------
def connectionInfo():
    print("Opening connection.")
    print("Server Host = %s" %(serverHost))
    print("Control Port = %s" %(serverPortNum))
    print("Command = %s" %(command))
    if fileName != None:
        print("File Name = %s" %(fileName))
    print("Data Port = %s" %(dataPortNum))


def connectDataSocket(hostName, portNum):
    dataSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    dataSocket.bind( (hostName, portNum))
    dataSocket.listen(1)
    return dataSocket


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def connectToServer(hostName, portNum):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect( (hostName, portNum) )
    return sock

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def sendDataPort(sock, dataPort):
    number = struct.pack('i', dataPort)
    sock.send(number)
    msg = sock.recv(3)
    msg = msg.decode().strip('\x00')

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def sendCommand(sock, cmd):
    cmdMsg = bytearray(cmd + "\0", encoding="UTF-8")
    sock.sendall(cmdMsg)
    msg = sock.recv(16)
    msg = msg.decode().strip('\x00')
    if msg != "OK":
        print("ERROR: Received from server: %s" %msg)
        return -1
    else:
        return 1



def sendOK(sock):
    okMsg = bytearray("OK\0", encoding="UTF-8")
    sock.sendall(okMsg)

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def receiveDir(sock):
    numFiles = sock.recv(4)
    numFiles = int.from_bytes(numFiles, byteorder='little')
    print("There are %d files:" %numFiles)
    sendOK(sock)

    for i in range(0, numFiles):
        files = sock.recv(512)
        files = files.decode().strip('\x00')
        print(files)
        sendOK(sock)


# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def receiveFile(sock):
    fileSize = sock.recv(4)
    fileSize = int.from_bytes(fileSize, byteorder='little')
    print("File size is %d" %fileSize)
    sendOK(sock)

    numBlocks = sock.recv(4)
    numBlocks = int.from_bytes(numBlocks, byteorder='little')
    print("Total Blocks is: %d" %numBlocks)
    sendOK(sock)

    fp = open(fileName, "wb")
    bytesReceived = 0

    for i in range(0, numBlocks):
        temp = sock.recv(1024)
        sendOK(sock)
        bytesReceived += len(temp)
        fp.write(temp)
        #print("Received %d of %d bytes" %(bytesReceived, fileSize) )

    print("File Received!")
    close(fp)



# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
if __name__ == "__main__":
    # validateInput
    if len(sys.argv) not in (5, 6):
        properOutput()
        exit(1)

    parseCommand()
    #connectionInfo()

    server = connectToServer(serverHost, serverPortNum)
    dataSock = connectDataSocket(serverHost, dataPortNum)

    sendDataPort(server, dataPortNum)

    if sendCommand(server, command) == -1:
        print("Exiting client")
        server.close()
        exit(1)

    if command == "-g":
        if os.path.exists(fileName):
            print("File %s Already exists. Overwrite?" %fileName)
            response = input("Y/n:")
            if response not in VALID_YES:
                print("Closing Connection")
                server.close()
                exit(1)
            else:
                # send file name
                sendCommand(server, fileName)
        else:
            # send file name
            if sendCommand(server, fileName) == -1:
                print("Exiting client")
                server.close()
                exit(1)

    dataSocket, dataAddr = dataSock.accept()

    if command == "-l":
        receiveDir(dataSocket)

    if command == "-g":
        receiveFile(dataSocket)

    print("Closing Connection")
    dataSock.close()
    dataSocket.close()
    server.close()
