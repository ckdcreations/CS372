/*******************************************************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS372 - Project #2
 * File Name: ftserver.c
 *******************************************************************************************************************/

// Include Libraries
#include <dirent.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// SIZE macro. 75000 is about 10k more than the largest file
#ifndef SIZE
#define SIZE 75000
#endif

// Error function used for reporting issues
void error(const char *msg) {
  perror(msg);
  exit(1);
}

int createSocket() {
  ///// Create our listening socket, check if created successfully /////
  int createSocket = socket(AF_INET, SOCK_STREAM, 0);
  if (createSocket < 0) {
    error("ERROR: Could not create socket!");
  }

  ///// Return our new listening socket /////
  return createSocket;
}

void initializeServerAddress(struct sockaddr_in *serverAddress, int portNum) {
  ////// Setup the server struct with port and other info /////
  memset((char *)serverAddress, '\0', sizeof(*serverAddress));
  serverAddress->sin_port = htons(portNum);
  serverAddress->sin_addr.s_addr = INADDR_ANY;
  serverAddress->sin_family = AF_INET;
}

void bindSocket(int sockFD, struct sockaddr_in *serverAddress) {
  ///// Bind server socket /////
  int bindSock =
      bind(sockFD, (struct sockaddr *)serverAddress, sizeof(*serverAddress));
  if (bindSock < 0) {
    error("ERROR: Failed to bind port");
  }
}

void receiveClientControl(int sockFD, int *controlSock,
                          struct sockaddr_in *clientAddress) {
  ///// This accepts a new client when a request comes in, and sets the control
  /// socket file descriptor /////
  socklen_t clientLength = sizeof(*clientAddress);
  *controlSock =
      accept(sockFD, (struct sockaddr *)clientAddress, &clientLength);
}

int receiveClientData(int portNum, struct sockaddr_in *controlAddress,
                      struct sockaddr_in *dataAddress) {
  ///// Create a new socket for data /////
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    error("ERROR: Could not create data socket");
  }

  ///// Set up the server parameters, specifically the port send in /////
  memset((char *)dataAddress, '\0', sizeof(*dataAddress));
  dataAddress->sin_port = htons(portNum);
  memcpy(&dataAddress->sin_addr, &controlAddress->sin_addr,
         sizeof(controlAddress->sin_addr));
  dataAddress->sin_family = AF_INET;

  ///// Make the connection and verify it didn't fail. /////
  int connectSock =
      connect(sock, (struct sockaddr *)dataAddress, sizeof(*dataAddress));
  if (connectSock < 0) {
    error("ERROR: Connection failed");
  }

  ///// Return our new data transfer socket file descriptor /////
  return sock;
}

/*
int createServer(int portNum) {

  int sockFD;
  struct sockaddr_in serverAddress;

  // Set up the address struct for this process (the server)
  memset((char *)&serverAddress, '\0',
         sizeof(serverAddress));           // Clear out the address struct
  serverAddress.sin_family = AF_INET;      // Create a network-capable socket
  serverAddress.sin_port = htons(portNum); // Store the port number
  serverAddress.sin_addr.s_addr =
      INADDR_ANY; // Any address is allowed for connection to this process

  // Set up the socket
  sockFD = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
  if (sockFD < 0)
    error("ERROR opening socket");

  // Enable the socket to begin listening
  if (bind(sockFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) <
      0) // Connect socket to port
    error("ERROR on binding");
  listen(sockFD,
         5); // Flip the socket on - it can now receive up to 5 connections
  fprintf(stdout, "Server started on port %d.\n", portNum);

  return sockFD;
}
*/

int getDataPort(int socket) {
  int num;
  ssize_t n = 0;

  n = read(socket, &num, sizeof(int));

  if (n < 0)
    error("ERROR receiving data port");

  write(socket, "OK", 3);

  return num;
}

int getCommand(int socket) {
  char cmd[3];
  ssize_t n = 0;
  ssize_t message = 0;

  while (message < 3) {
    n = read(socket, &cmd + message, 3 - message);
    message += n;

    if (n < 0)
      error("ERROR receiving data port");
  }

  if (strcmp(cmd, "-l") == 0) {
    write(socket, "OK", 3);
    return 1;
  } else if (strcmp(cmd, "-g") == 0) {
    write(socket, "OK", 3);
    return 2;
  } else {
    write(socket, "Invalid Command", 16);
    return -1;
  }
}

void getFileName(int socket, char fileName[]) {
  ssize_t n = 0;
  ssize_t message = 0;

  n = read(socket, &fileName, 512);

  if (n < 0)
    error("ERROR receiving data port");

  write(socket, "OK", 3);
}

int readDirectory(char *dirPath[]) {
  DIR *d;
  struct dirent *dir;
  int i = 0;
  int numFiles = 0;

  d = opendir(".");
  if (d != 0) {
    while ((dir = readdir(d)) != NULL) {
      if (dir->d_type == DT_REG) {
        dirPath[i] = dir->d_name;
        i++;
      }
    }

    // Iterator will be one larger than the number of files
    numFiles = i - 1;
  }

  closedir(d);

  return numFiles;
}

void receiveOK(int socket) {
  char buf[3];
  read(socket, buf, sizeof(buf));
  fprintf(stdout, "Message from client: %s\n", buf);
}

void sendDirectory(int socket, char *dirPath[]) {
  int numFiles = readDirectory(dirPath);

  ssize_t n = 0;

  fprintf(stdout, "number of files: %d\n", numFiles);
  n = write(socket, &numFiles, sizeof(int));
  receiveOK(socket);

  n = 0;
  int i = 0;

  for (i = 0; i < numFiles; i++) {
    write(socket, dirPath[i], strlen(dirPath[i]));
    receiveOK(socket);
  }
}

void sendFile(int socket, char *fileName) {
  FILE *fp;
  // Using long int because of the need for very large files
  int fileSize;
  fp = fopen(fileName, "rb");

  if (fp != NULL) {
    fseek(fp, 0L, SEEK_END);
    fileSize = ftell(fp);
    rewind(fp);
  }

  fprintf(stdout, "File size is %d\n", fileSize);
  write(socket, &fileSize, sizeof(int));
  receiveOK(socket);

  int blockSize = 1024;
  int numBlocks = fileSize / blockSize;
  int remain = fileSize % blockSize;
  int totalBlocks = numBlocks + remain;
  char buffer[blockSize];
  int i;

  write(socket, &totalBlocks, sizeof(int));
  receiveOK(socket);

  for (i = 0; i < numBlocks; i++) {
    fread(buffer, sizeof(buffer), 1, fp);
    write(socket, buffer, 1024);
    receiveOK(socket);
  }

  if (remain > 0) {
    fread(buffer, remain, 1, fp);
    write(socket, buffer, remain);
    receiveOK(socket);
  }

  fclose(fp);
}

/******************************************************************************************************************
 * Function: main
 * Description:
 ********************************************************************************************************************/
int main(int argc, char *argv[])

{
  int listenSocketFD, controlSocketFD, dataSocketFD, serverPortNumber,
      dataPortNumber, command;
  socklen_t sizeOfClientInfo;
  char buffer[SIZE];
  char fileName[512];
  char *directory[100];
  struct sockaddr_in serverAddress, clientControlAddress, clientDataAddress;
  // The child process ID will be used for file creation
  pid_t chPID;

  if (argc < 2) {
    fprintf(stderr, "USAGE: %s port\n", argv[0]);
    exit(1);
  } // Check usage & args

  memset(fileName, '\0', 512);

  serverPortNumber = atoi(argv[1]);
  if (serverPortNumber < 1024 || serverPortNumber > 65535)
    error("ERROR: Port number out of range. Please select a value between 1024 "
          "and 65535");

  listenSocketFD = createSocket();
  initializeServerAddress(&serverAddress, serverPortNumber);
  bindSocket(listenSocketFD, &serverAddress);

  // The while loop will keep the server alive forever. It will require a
  // signal command to quit.
  while (1) {

    listen(listenSocketFD, 1);

    receiveClientControl(listenSocketFD, &controlSocketFD,
                         &clientControlAddress);

    if (controlSocketFD < 0)
      error("ERROR: Client connection failed");

    // When a new connection is made, fork to a new process. This way, the
    // server can keep taking new connections
    chPID = fork();
    if (chPID < 0) {
      fprintf(stderr, "Server: ERROR while forking process");
      exit(1);
    }

    if (chPID == 0) {

      fprintf(stdout, "New connection established.\n");

      dataPortNumber = getDataPort(controlSocketFD);
      fprintf(stdout, "Data port: %d\n", dataPortNumber);

      command = getCommand(controlSocketFD);
      fprintf(stdout, "Command is: %d\n", command);

      if (command == 2) {
        // getFileName(controlSocketFD, &fileName);
        read(controlSocketFD, fileName, 512);
        write(controlSocketFD, "OK", 3);
        fprintf(stdout, "FileName: %s\n", fileName);
        // sendFile();
      }

      dataSocketFD = receiveClientData(dataPortNumber, &clientControlAddress,
                                       &clientDataAddress);
      fprintf(stdout, "Now connected to client data socket\n");

      if (command == 1) {
        // sendDirectory(dataSocketFD, directory);
        sendDirectory(dataSocketFD, directory);
      }

      else if (command == 2) {
        sendFile(dataSocketFD, fileName);
      }

      // Close the socket
      close(dataSocketFD);
      close(controlSocketFD);
      fprintf(stdout, "Closing the connection.\n");
    }
  }

  close(listenSocketFD); // Close the listening socket

  return 0;
}
