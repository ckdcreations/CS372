Name: Clifford Dunn
Assignment: CS 372 - Project 2
Description: Simple FTP Server/Client


After file unzip, simply run make. This will compile the server.
Then run server by typing ./ftserver <portNum>

To execute the client:
cd client
./ftclient.py <host> <serverPortNum> <command> [fileName] <dataPortNum>

EXTRA CREDIT: The program can transmit any type of file.


Refrences:

Beej's guide was essential for socket programming

CS344 Project #4 was useful for the C server

This was crucial for figuring out how to convert data types with python:
https://docs.python.org/3.0/whatsnew/3.0.html#text-vs-data-instead-of-unicode-vs-8-bit

This was crucial for figuring out socket programming with python:
https://docs.python.org/3/library/socket.html

This github account was helpful, though still convoluted for me:
https://github.com/caperren/CS372/tree/master/Project%202
