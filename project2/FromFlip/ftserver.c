
/*******************************************************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS372 - Project #2
 * File Name: ftserver.c
 *******************************************************************************************************************/

// Include Libraries
#include <dirent.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// SIZE macro. 75000 is about 10k more than the largest file
#ifndef SIZE
#define SIZE 75000
#endif

// Error function used for reporting issues
void error(const char *msg) {
  perror(msg);
  exit(1);
}

/*******************************************************************************************************************
 * Function: createSocket
 * Input: None
 * Output: a socket is created for network communication
*******************************************************************************************************************/
int createSocket() {
  ///// Create our listening socket, check if created successfully /////
  int createSocket = socket(AF_INET, SOCK_STREAM, 0);
  if (createSocket < 0) {
    error("ERROR: Could not create socket!");
  }

  ///// Return our new listening socket /////
  return createSocket;
}

/*******************************************************************************************************************
 * Function: initializeServerAddress 
 * Input: serverAddress, portNum
 * Output: This prepares the adress for communication. By initilizing the port with AF and ADDR settings, the socket
 *         will be created
 * Note: Special thanks to Professor Brewster and CS344
********************************************************************************************************************/
void initializeServerAddress(struct sockaddr_in *serverAddress, int portNum) {
  ////// Setup the server struct with port and other info /////
  memset((char *)serverAddress, '\0', sizeof(*serverAddress));
  serverAddress->sin_port = htons(portNum);
  serverAddress->sin_addr.s_addr = INADDR_ANY;
  serverAddress->sin_family = AF_INET;
}

/*******************************************************************************************************************
 * Function: bindSocket
 * Input: sockFD, serverAddress
 * Output: This takes the server address and socket which were previously created and combins them binding them to
 *         a port. 
*******************************************************************************************************************/
void bindSocket(int sockFD, struct sockaddr_in *serverAddress) {
  ///// Bind server socket /////
  int bindSock =
      bind(sockFD, (struct sockaddr *)serverAddress, sizeof(*serverAddress));
  if (bindSock < 0) {
    error("ERROR: Failed to bind port");
  }
}

/*******************************************************************************************************************
 * Function: receiveClientControl
 * Input: sockFD, controlSock, clientAddress
 * Output: Receives information from the client. This gets the client's address and binds to the control socket
*******************************************************************************************************************/
void receiveClientControl(int sockFD, int *controlSock,
                          struct sockaddr_in *clientAddress) {
  ///// This accepts a new client when a request comes in, and sets the control
  /// socket file descriptor /////
  socklen_t clientLength = sizeof(*clientAddress);
  *controlSock =
      accept(sockFD, (struct sockaddr *)clientAddress, &clientLength);
}

/*******************************************************************************************************************
 * Function: receiveClient
 * Input: portNum, controlAddress, dataAddress
 * Output: Receives information from the client. This gets the client's data port and binds a connection to it
*******************************************************************************************************************/
int receiveClientData(int portNum, struct sockaddr_in *controlAddress,
                      struct sockaddr_in *dataAddress) {
  ///// Create a new socket for data /////
  int sock = socket(AF_INET, SOCK_STREAM, 0);
  if (sock < 0) {
    error("ERROR: Could not create data socket");
  }

  ///// Set up the server parameters, specifically the port send in /////
  memset((char *)dataAddress, '\0', sizeof(*dataAddress));
  dataAddress->sin_port = htons(portNum);
  memcpy(&dataAddress->sin_addr, &controlAddress->sin_addr,
         sizeof(controlAddress->sin_addr));
  dataAddress->sin_family = AF_INET;

  ///// Make the connection and verify it didn't fail. /////
  int connectSock =
      connect(sock, (struct sockaddr *)dataAddress, sizeof(*dataAddress));
  if (connectSock < 0) {
    error("ERROR: Connection failed");
  }

  ///// Return our new data transfer socket file descriptor /////
  return sock;
}

/*******************************************************************************************************************
 * Function: getDataPort
 * Input: socket
 * Output: Returns the number for the data port
*******************************************************************************************************************/
int getDataPort(int socket) {
  int num;
  ssize_t n = 0;

  // Read the data from the socket and ensure that the reading did not fail.
  n = read(socket, &num, sizeof(int));

  if (n < 0)
    error("ERROR receiving data port");

  // Send a confirmation to the client
  write(socket, "OK", 3);

  return num;
}

/*******************************************************************************************************************
 * Function: getCommand
 * Input: socket
 * Output: This receives commands through the socket and sends confirmation. Also sends fail signals when necessary
*******************************************************************************************************************/
int getCommand(int socket) {
  // A simple command is sent from the client. This is a small 3 char command.
  char cmd[3];
  ssize_t n = 0;
  ssize_t message = 0;

  // Read the command coming in.
  while (message < 3) {
    n = read(socket, &cmd + message, 3 - message);
    message += n;

    // Verify that the reading was correct.
    if (n < 0)
      error("ERROR receiving data port");
  }

  // Send confirmation to the client if the command sent was valid. 
  if (strcmp(cmd, "-l") == 0) {
    write(socket, "OK", 3);
    return 1;
  } else if (strcmp(cmd, "-g") == 0) {
    write(socket, "OK", 3);
    return 2;
    // Otherwise, send a message back to the client that it was not
  } else {
    write(socket, "Invalid Command", 16);
    return -1;
  }
}



/*******************************************************************************************************************
 * Function: readDirectory
 * Input: dirPath
 * Output: Reads through the current directory and places the names of files in the dirPath array
*******************************************************************************************************************/
int readDirectory(char *dirPath[]) {
  // Create the directory struct and variables.
  DIR *d;
  struct dirent *dir;
  int i = 0;
  int numFiles = 0;

  // Loop through the directory and add names of files to the dirPath
  d = opendir(".");
  if (d != 0) {
    while ((dir = readdir(d)) != NULL) {
      if (dir->d_type == DT_REG) {
        dirPath[i] = dir->d_name;
        i++;
      }
    }

    // Iterator will be one larger than the number of files
    numFiles = i - 1;
  }

  closedir(d);

  // Keep track of how many files are out there.
  return numFiles;
}

/*******************************************************************************************************************
 * Function: receiveOK
 * Input: socket
 * Output: Receives an OK signal from the client
*******************************************************************************************************************/
void receiveOK(int socket) {
  char buf[3];
  read(socket, buf, sizeof(buf));
  fprintf(stdout, "Message from client: %s\n", buf);
}

/*******************************************************************************************************************
 * Function: sendDirectory
 * Input: socket, dirPath
 * Output: Sends the contents of the directory over the socket to the client
*******************************************************************************************************************/
void sendDirectory(int socket, char *dirPath[]) {
  // Call the readDirectory function to make sure the path is filled
  int numFiles = readDirectory(dirPath);

  ssize_t n = 0;

  // Print the number of files. Send the number of files to the client
  fprintf(stdout, "number of files: %d\n", numFiles);
  n = write(socket, &numFiles, sizeof(int));
  // Receive confirmation
  receiveOK(socket);

  n = 0;
  int i = 0;

  // Send each file name to the client individually
  for (i = 0; i < numFiles; i++) {
    write(socket, dirPath[i], strlen(dirPath[i]));
    receiveOK(socket);
  }
}

/*******************************************************************************************************************
 * Function: sendFile
 * Input: socket, fileName
 * Output: Sends the file over the socket to the client
*******************************************************************************************************************/
void sendFile(int socket, char *fileName) {
  // Create the file pointer
  FILE *fp;
  int fileSize;
  // Note, the "rb" command makes it possible to send full binary data. This allows any file type to be transferred
  // As a test, I have included the file LeClair.flac
  fp = fopen(fileName, "rb");

  // Determine the file size
  if (fp != NULL) {
    fseek(fp, 0L, SEEK_END);
    fileSize = ftell(fp);
    rewind(fp);
  }

  // Print the file size and send/recv the data to the socket
  fprintf(stdout, "File size is %d\n", fileSize);
  write(socket, &fileSize, sizeof(int));
  receiveOK(socket);

  // Standard block size of 1024. This should be fast and safe enough for data transfer
  int blockSize = 1024;
  int numBlocks = fileSize / blockSize;
  int remain = fileSize % blockSize;
  int totalBlocks = numBlocks + remain;
  char buffer[blockSize];
  memset(buffer, '\0', blockSize);
  int i;

  // Inform the client how many total blocks will be coming. Receive confirmation
  write(socket, &totalBlocks, sizeof(int));
  receiveOK(socket);

  // The for loop allocates all full blocks to be written to the socket.
  // Each iteration zeros out the buffer so the file is always filled with
  // the accurate file data. Send each block as they are ready
  for (i = 0; i < numBlocks; i++) {
    memset(buffer, '\0', blockSize);
    fread(buffer, sizeof(buffer), 1, fp);
    write(socket, buffer, 1024);
    receiveOK(socket);
  }

  // In the instance that the size of the file doesn't divide perfectly into the blocks, send one more file with the 
  // extra data so the file is complete.
  if (remain > 0) {
    memset(buffer, '\0', blockSize);
    fread(buffer, remain, 1, fp);
    write(socket, buffer, remain);
    receiveOK(socket);
  }

  fclose(fp);
  fprintf(stdout, "Transfer of %s complete!\n", fileName);
}

/******************************************************************************************************************
 * Function: main
 ********************************************************************************************************************/
int main(int argc, char *argv[])

{
  // Declare variables
  int listenSocketFD, controlSocketFD, dataSocketFD, serverPortNumber,
      dataPortNumber, command;
  socklen_t sizeOfClientInfo;
  char buffer[SIZE];
  char fileName[512];
  char *directory[100];
  struct sockaddr_in serverAddress, clientControlAddress, clientDataAddress;
  // The child process ID will be used for the fork 
  pid_t chPID;

  if (argc < 2) {
    fprintf(stderr, "USAGE: %s port\n", argv[0]);
    exit(1);
  } // Check usage & args

  // Zero out the buffer for fileName
  memset(fileName, '\0', 512);

  // Ensure that the control port is valid
  serverPortNumber = atoi(argv[1]);
  if (serverPortNumber < 1024 || serverPortNumber > 65535)
    error("ERROR: Port number out of range. Please select a value between 1024 "
          "and 65535");

  // Create the initial socket will will be used for control
  listenSocketFD = createSocket();
  initializeServerAddress(&serverAddress, serverPortNumber);
  bindSocket(listenSocketFD, &serverAddress);

  // The while loop will keep the server alive forever. It will require a
  // signal command to quit.
  while (1) {

    // Listen for a new connection
    listen(listenSocketFD, 1);

    // When a client connects, begin gathering data
    receiveClientControl(listenSocketFD, &controlSocketFD,
                         &clientControlAddress);

    // Make sure ther ewas no error connecting to the client
    if (controlSocketFD < 0)
      error("ERROR: Client connection failed");

    // When a new connection is made, fork to a new process. This way, the
    // server can keep taking new connections
    chPID = fork();
    if (chPID < 0) {
      fprintf(stderr, "Server: ERROR while forking process");
      exit(1);
    }

    // New connection
    if (chPID == 0) {

      fprintf(stdout, "New connection established.\n");

      // Retreive the data port number and print it
      dataPortNumber = getDataPort(controlSocketFD);
      fprintf(stdout, "Data port: %d\n", dataPortNumber);

      // Retreive the command from the client
      command = getCommand(controlSocketFD);


      /* 
       * This collection of if statements creates the functionality for the entire program
       * The first if statement is used if the client requests a file. In this instance, the
       * server makes sure that the file exists.
       */
      if (command == 2) {
        FILE *fp;
        read(controlSocketFD, fileName, 512);
        if (fp = fopen(fileName, "r")) {
          fclose(fp);
          write(controlSocketFD, "OK", 3);
          fprintf(stdout, "FileName: %s\n", fileName);
        } else {
          write(controlSocketFD, "File not found.", 16);
          command = -1;
        }
      }

      /*
       * The second if statement is just making sure there are no problems with the client's
       * request. When there are no issues, the server will connect to the proper data socket
       */
      if (command != -1) {
        dataSocketFD = receiveClientData(dataPortNumber, &clientControlAddress,
                                         &clientDataAddress);
        fprintf(stdout, "Now connected to client data socket\n");
      }

      /*
       * The third if statement is simply there to send the directory if that's the choice that was made
       */
      if (command == 1) {
        sendDirectory(dataSocketFD, directory);
      }

      /* 
       * The final if statement requires the same condition as the first. However, this time, the file
       * will actually be sent to the client
       */
      else if (command == 2) {
        sendFile(dataSocketFD, fileName);
      }

      // Close the socket
      close(dataSocketFD);
      close(controlSocketFD);
      fprintf(stdout, "Closing the connection.\n");
    }
  }

  close(listenSocketFD); // Close the listening socket

  return 0;
}
