#!/bin/python3

# ----------------------------------------------------------------------------
# Name: Clifford Dunn
# Description: OSU CS372 - Project #2
# File Name: ftclient.py
# Language: Python 3
# EXTRA CREDIT: Program Pair can send any file (not just txt)
# ----------------------------------------------------------------------------

# Important Libraries
import socket
import sys
import os
from time import sleep
from signal import signal, SIGPIPE, SIG_DFL
import struct
signal(SIGPIPE, SIG_DFL)



# To be used for overwriting files
VALID_YES = ["", "Y", "y"]


# ----------------------------------------------------------------------------
# Function: properOutput
# Input: None
# Output: Displays the proper use of the command line syntax
# ----------------------------------------------------------------------------
def properOutput():
    print("Guidelines for ftclient.py:")
    print("This program expects either five or six arguments")
    print("Example 1: This command will list the directory")
    print("./ftclient.py <serverHost> <portNum> -l <dataPortNum>")
    print("Example 2: This command will request a file from the directory")
    print("./ftclient.py <serverHost> <portNum> -g  <fileName> <dataPortNum>")
    print("NOTE: If connecting to an OSU server, simply use flip1, flip2, or flip3 for <serverHost>")

# ----------------------------------------------------------------------------
# Function: parseCommand
# Input: From command line: serverHost, serverPortNum, command, fileName,
#        dataPortNum
# Output: Verifies each argument to make sure the program is run correctly.
# ----------------------------------------------------------------------------
def parseCommand():
    # Global variables are used making this function really more of a macro for ease
    # of validation
    global serverHost
    global serverPortNum
    global command
    global fileName
    global dataPortNum

    # Can connect to any web address, but has prebuilt functionality with the OSU flip servers
    if (sys.argv[1] == "flip1") or (sys.argv[1] == "flip2") or (sys.argv[1] == "flip3"):
        serverHost = sys.argv[1] + ".engr.oregonstate.edu"
    else:
        serverHost = sys.argv[1]

    # Verifies that the desired port number is in range
    serverPortNum = int(sys.argv[2])
    if serverPortNum not in range(1024, 65536):
        print("ERROR: Server port number out of range. Please select a port number between 1024 and 65535")
        exit(1)

    # Assign the command variable. Will accept any input
    command = sys.argv[3]

    # If the command is -g (get), assign the fileName variable
    if command == "-g":
        fileName = sys.argv[4]
    else:
        fileName = None


    # [-1] takes the last argument in the array
    dataPortNum = int(sys.argv[-1])
    if dataPortNum not in range(1024, 65536):
        print("ERROR: Data port number out of range. Please select a port number between 1024 and 65535")
        exit(1)

    if serverPortNum == dataPortNum:
        print("ERROR: Can not use the same port for server and data")
        exit(1)


# ----------------------------------------------------------------------------
# Function: connectionInfo
# Input: None
# Output: Prints the information that is being used to connect and command
#         the server
# ----------------------------------------------------------------------------
def connectionInfo():
    # Prints all the information for connection.
    print("Opening connection.")
    print("Server Host = %s" %(serverHost))
    print("Control Port = %s" %(serverPortNum))
    print("Command = %s" %(command))
    if fileName != None:
        print("File Name = %s" %(fileName))
    print("Data Port = %s" %(dataPortNum))


# ----------------------------------------------------------------------------
# Function: connectDataSocket
# Input: hostName, portNum
# Output: Opens a new socket that the server will connect to for data transfer
# ----------------------------------------------------------------------------
def connectDataSocket(hostName, portNum):
    dataSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    dataSocket.bind( (hostName, portNum))
    dataSocket.listen(1)
    return dataSocket


# ----------------------------------------------------------------------------
# Function: connectToServer
# Input: hostName, portNum
# Output: Opens and connects to a socket at a server location
# ----------------------------------------------------------------------------
def connectToServer(hostName, portNum):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect( (hostName, portNum) )
    return sock

# ----------------------------------------------------------------------------
# Function: sendDataPort
# Input: sock, dataPort
# Output: Connects to a separate port on the server and creates a data
#         connection
# ----------------------------------------------------------------------------
def sendDataPort(sock, dataPort):
    number = struct.pack('i', dataPort)
    sock.send(number)
    msg = sock.recv(3)
    # Incoming data is in binary format. This will decode it
    msg = msg.decode().strip('\x00')


# ----------------------------------------------------------------------------
# Function: sendCommand
# Input: sock, cmd
# Output: Connects to a socket and sends data as cmd variable
# ----------------------------------------------------------------------------
def sendCommand(sock, cmd):
    # Take the input command and convert it into a byte array
    cmdMsg = bytearray(cmd + "\0", encoding="UTF-8")
    # Send the byte array
    sock.sendall(cmdMsg)
    # Receive confirmation 
    msg = sock.recv(16)
    # Convert the confirmation message
    msg = msg.decode().strip('\x00')
    # If the message is not acceptable, print an error message from the
    # server and return -1
    if msg != "OK":
        print("ERROR: Received from server: %s" %msg)
        return -1
    # Otherwise return 1
    else:
        return 1



# ----------------------------------------------------------------------------
# Function: sendOK
# Input: sock
# Output: Sends a message through the socket to verify transmission
# ----------------------------------------------------------------------------
def sendOK(sock):
    okMsg = bytearray("OK\0", encoding="UTF-8")
    sock.sendall(okMsg)

# ----------------------------------------------------------------------------
# Function: receiveDir
# Input: sock
# Output: A connection will be made with the server. The server will send a
#         list of files in its main directory
# ----------------------------------------------------------------------------
def receiveDir(sock):
    # First, receive the total number of files in the server directory
    numFiles = sock.recv(4)
    # Convert the byte array to an integer
    numFiles = int.from_bytes(numFiles, byteorder='little')
    # Inform the user how many files are in the directory
    print("There are %d files:" %numFiles)
    # Send confirmation to the server
    sendOK(sock)

    # Loop through the connection and print the names of each file on
    # the server. At each receive, send an OK back to the server.
    for i in range(0, numFiles):
        files = sock.recv(512)
        files = files.decode().strip('\x00')
        print(files)
        sendOK(sock)


# ----------------------------------------------------------------------------
# Function: receiveFile
# Input: sock
# Output: Receives the specified file from the server
# ----------------------------------------------------------------------------
def receiveFile(sock):
    # Receive the size of the file
    fileSize = sock.recv(4)
    # Convert to int
    fileSize = int.from_bytes(fileSize, byteorder='little')
    # Inform the user of file size
    print("File size is %d" %fileSize)
    sendOK(sock)

    # Receive how many blocks will be sent
    numBlocks = sock.recv(4)
    # Inform the user how many blocks will be sent
    numBlocks = int.from_bytes(numBlocks, byteorder='little')
    print("Total Blocks is: %d" %numBlocks)
    sendOK(sock)

    # Open a file and prepare to fill it. The "wb" format allows for binary
    # data creation. This means any file can be sent.
    fp = open(fileName, "wb")
    bytesReceived = 0

    # For loop to receive data. Each block is sent and confirmed. Each block
    # Is written to the file. Once finished, the file is closed
    for i in range(0, numBlocks):
        temp = sock.recv(1024)
        sendOK(sock)
        bytesReceived += len(temp)
        fp.write(temp)
        #print("Received %d of %d bytes" %(bytesReceived, fileSize) )

    print("File Received!")
    close(fp)



# ---------------------------------------------------------------------------------
# Function: Main
# Input: From command line: ./ftclient.py <host> <ctrlPort> <-l || -g> <dataPort>
# ---------------------------------------------------------------------------------
if __name__ == "__main__":
    # validateInput
    if len(sys.argv) not in (5, 6):
        properOutput()
        exit(1)

    # Parse each value from command line and place into variables
    parseCommand()
    #connectionInfo()

    # Connect to the server and set up both required sockets
    server = connectToServer(serverHost, serverPortNum)
    dataSock = connectDataSocket(serverHost, dataPortNum)

    # Send the port to the server that will be used for data transfer
    sendDataPort(server, dataPortNum)

    # Send the command from the command line to the server
    # If the server sends back an unknown command error, 
    # disconnect and quit program
    if sendCommand(server, command) == -1:
        print("Exiting client")
        server.close()
        exit(1)

    # If the command sent was the -g (get) command, first make sure
    # that the sought file is not already in the current directory.
    # If it does exist, prompt the user to replace. 
    if command == "-g":
        if os.path.exists(fileName):
            print("File %s Already exists. Overwrite?" %fileName)
            response = input("Y/n:")
            # If the user doesn't want to overwrite the file, 
            # close and exit the program
            if response not in VALID_YES:
                print("Closing Connection")
                server.close()
                exit(1)
            # Otherwise, replace the file
            else:
                # send file name
                sendCommand(server, fileName)
        # If the file name does not already exist,
        # send the name of the file to the server
        else:
            # Send file name. If the file doesn't exist,
            # quit the program.
            if sendCommand(server, fileName) == -1:
                print("Exiting client")
                server.close()
                exit(1)

    # Accept the connection on the data socket
    dataSocket, dataAddr = dataSock.accept()

    # If the original command was -l (list), display the
    # available contents of the server directory
    if command == "-l":
        receiveDir(dataSocket)

    # If the command was -g (get), download the file
    # from the server
    if command == "-g":
        receiveFile(dataSocket)

    # Close and exit the program
    print("Closing Connection")
    dataSock.close()
    dataSocket.close()
    server.close()
