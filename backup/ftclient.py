#!/bin/python2

# ----------------------------------------------------------------------------
# Name: Clifford Dunn
# Description: OSU CS372 - Project #2
# File Name: ftclient.py
# Language: Python 2
# ----------------------------------------------------------------------------

# Important Libraries
import socket
import sys
import os
from time import sleep
import struct

# ----------------------------------------------------------------------------
# Function: properOutput
# Input: None
# Output: Displays the proper use of the command line syntax
# ----------------------------------------------------------------------------
def properOutput():
    print("Guidelines for ftclient.py:")
    print("This program expects either five or six arguments")
    print("Example 1: This command will list the directory")
    print("./ftclient.py <serverHost> <portNum> -l <dataPortNum>")
    print("Example 2: This command will request a file from the directory")
    print("./ftclient.py <serverHost> <portNum> -g  <fileName> <dataPortNum>")
    print("NOTE: If connecting to an OSU server, simply use flip1, flip2, or flip3 for <serverHost>")

# ----------------------------------------------------------------------------
# Function: parseCommand
# Input: From command line: serverHost, serverPortNum, command, fileName,
#        dataPortNum
# Output: Verifies each argument to make sure the program is run correctly.
# ----------------------------------------------------------------------------
def parseCommand():
    global serverHost
    global serverPortNum
    global command
    global fileName
    global dataPortNum

    if (sys.argv[1] == "flip1") or (sys.argv[1] == "flip2") or (sys.argv[1] == "flip3"):
        serverHost = sys.argv[1] + ".engr.oregonstate.edu"
    else:
        serverHost = sys.argv[1]

    serverPortNum = int(sys.argv[2])
    if serverPortNum not in range(1024, 65536):
        print("ERROR: Server port number out of range. Please select a port number between 1024 and 65535")
        exit(1)

    command = sys.argv[3]

    if command == "-g":
        fileName = sys.argv[4]
    else:
        fileName = None

    if command not in ("-l","-g"):
        print("ERROR: Unknown command")
        exit(1)

    if command == "-g" and len(sys.argv) == 5:
        print("ERROR: Improper use of -g command")
        exit(1)
    if command == "-l" and len(sys.argv) == 6:
        print("ERROR: Improper use of -l command")
        exit(1)


    # [-1] takes the last argument in the array
    dataPortNum = int(sys.argv[-1])
    if dataPortNum not in range(1024, 65536):
        print("ERROR: Data port number out of range. Please select a port number between 1024 and 65535")
        exit(1)

    if serverPortNum == dataPortNum:
        print("ERROR: Can not use the same port for server and data")
        exit(1)


# ----------------------------------------------------------------------------
# Function: connectionInfo
# Input: None
# Output: Prints the information that is being used to connect and command
#         the server
# ----------------------------------------------------------------------------
def connectionInfo():
    print("Opening connection.")
    print("Server Host = %s" %(serverHost))
    print("Control Port = %s" %(serverPortNum))
    print("Command = %s" %(command))
    if fileName != None:
        print("File Name = %s" %(fileName))
    print("Data Port = %s" %(dataPortNum))

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def connectToServer(hostName, portNum):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect( (hostName, portNum) )
    return sock

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def sendDataPort(sock, dataPort):
    number = struct.pack('i', dataPort)
    sock.send(number)

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def receiveConfirm(sock):
    msg = sock.recv(20)
    print(msg)

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def sendCommand(sock, cmd):
    cmdMsg = bytearray(cmd + "\0", encoding="UTF-8")
    sock.sendall(cmdMsg)

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
def receiveDir(sock):
    directory = ""
    tempDir = ""

    numFiles = sock.recv(4)
    print(numFiles)

# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
if __name__ == "__main__":
    # validateInput
    if len(sys.argv) not in (5, 6):
        properOutput()
        exit(1)

    parseCommand()
    connectionInfo()

    server = connectToServer(serverHost, serverPortNum)

    sendCommand(server, command)
    sendDataPort(server, dataPortNum)

    dataSocket = socket.socket(socket.AF_INET,  socket.SOCK_STREAM)
    dataSocket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1);
    dataSocket.bind( (serverHost, dataPortNum) )
    dataSocket.listen(1)
    dataSock, addr = dataSocket.accept()


    receiveConfirm(dataSock)


    #receiveDir(dataSock)

    dataSock.close()
    server.close()
