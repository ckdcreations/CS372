/*******************************************************************************************************************
 * Name: Clifford Dunn
 * Description: OSU CS372 - Project #2
 * File Name: ftserver.c
 *******************************************************************************************************************/

// Include Libraries
#include <dirent.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

// SIZE macro. 75000 is about 10k more than the largest file
#ifndef SIZE
#define SIZE 75000
#endif

// Error function used for reporting issues
void error(const char *msg) {
  perror(msg);
  exit(1);
}

int createServer(int portNum) {

  int sockFD;
  struct sockaddr_in serverAddress;

  // Set up the address struct for this process (the server)
  memset((char *)&serverAddress, '\0',
         sizeof(serverAddress));           // Clear out the address struct
  serverAddress.sin_family = AF_INET;      // Create a network-capable socket
  serverAddress.sin_port = htons(portNum); // Store the port number
  serverAddress.sin_addr.s_addr =
      INADDR_ANY; // Any address is allowed for connection to this process

  // Set up the socket
  sockFD = socket(AF_INET, SOCK_STREAM, 0); // Create the socket
  if (sockFD < 0)
    error("ERROR opening socket");

  // Enable the socket to begin listening
  if (bind(sockFD, (struct sockaddr *)&serverAddress, sizeof(serverAddress)) <
      0) // Connect socket to port
    error("ERROR on binding");
  listen(sockFD,
         5); // Flip the socket on - it can now receive up to 5 connections
  fprintf(stdout, "Server started on port %d.\n", portNum);

  return sockFD;
}

int getDataPort(int socket) {
  int num;
  ssize_t n = 0;

  n = read(socket, &num, sizeof(int));

  if (n < 0)
    error("ERROR receiving data port");

  return num;
}

void sendConfirmation(int socket) {
  char *confirm = "Connected to server.";
  ssize_t n = 0;
  ssize_t confSize = strlen(confirm) + 1;
  ssize_t message = 0;

  while (message < confSize) {
    n = write(socket, confirm, confSize - message);
    message += n;

    if (n < 0)
      error("ERROR sending confirmation");
    else if (n == 0)
      message = confSize - message;
  }
}

int getCommand(int socket) {
  char cmd[3];
  ssize_t n = 0;
  ssize_t message = 0;

  while (message < 3) {
    n = read(socket, &cmd + message, 3 - message);
    message += n;

    if (n < 0)
      error("ERROR receiving data port");
  }

  if (strcmp(cmd, "-l") == 0)
    return 1;
  else if (strcmp(cmd, "-g") == 0)
    return 2;
}

int getDirectory(char *dirPath[]) {
  DIR *d;
  struct dirent *dir;
  int i = 0;
  int numFiles = 0;

  d = opendir(".");
  if (d != 0) {
    while ((dir = readdir(d)) != NULL) {
      if (dir->d_type == DT_REG) {
        dirPath[i] = dir->d_name;
        i++;
      }
    }

    // Iterator will be one larger than the number of files
    numFiles = i - 1;
  }

  closedir(d);

  return numFiles;
}

void sendDirectory(int socket, char *dirPath[]) {
  int numFiles = getDirectory(dirPath);

  ssize_t n = 0;

  fprintf(stdout, "number of files: %d\n", numFiles);
  n = write(socket, &numFiles, sizeof(int));
  /*
  ssize_t confSize = strlen(confSize) + 1;
  ssize_t message = 0;

        while (i < numFiles) {
          fprintf(stdout, "%s\n", directory[i]);
          fprintf(stdout, "%d\n", strlen(directory[i]));
          i++;
        }
  while (message < confSize) {
    n = write(socket, confirm, confSize - message);
    message += n;

    if (n < 0)
      error("ERROR sending confirmation");
    else if (n == 0)
      message = confSize - message;
  }
  */
}
/******************************************************************************************************************
 * Function: main
 * Description:
 ********************************************************************************************************************/
int main(int argc, char *argv[])

{
  int listenSocketFD, socketFD, clientDataSockFD, newSocketFD, serverPortNumber,
      dataPortNumber, command;
  socklen_t sizeOfClientInfo;
  char buffer[SIZE];
  char *directory[100];
  struct sockaddr_in serverAddress, clientAddress;
  // The child process ID will be used for file creation
  pid_t chPID;

  if (argc < 2) {
    fprintf(stderr, "USAGE: %s port\n", argv[0]);
    exit(1);
  } // Check usage & args

  serverPortNumber = atoi(argv[1]);
  listenSocketFD = createServer(serverPortNumber);

  // Accept a connection, blocking if one is not available until one connects
  sizeOfClientInfo = sizeof(clientAddress); // Get the size of the address for
                                            // the client that will connect

  // The while loop will keep the server alive forever. It will require a
  // signal command to quit.
  while (1) {
    socketFD = accept(listenSocketFD, (struct sockaddr *)&clientAddress,
                      &sizeOfClientInfo); // Accept
    if (socketFD < 0)
      error("ERROR on accept");

    // When a new connection is made, fork to a new process. This way, the
    // server can keep taking new connections

    chPID = fork();
    if (chPID < 0) {
      fprintf(stderr, "Server: ERROR while forking process");
      exit(1);
    }

    if (chPID == 0) {

      fprintf(stdout, "New connection established.\n");

      command = getCommand(socketFD);
      dataPortNumber = getDataPort(socketFD);
      fprintf(stdout, "Data port is %d\n", dataPortNumber);

      clientAddress.sin_family = AF_INET;
      clientAddress.sin_port = htons(dataPortNumber);
      clientDataSockFD = socket(AF_INET, SOCK_STREAM, 0);
      if (connect(clientDataSockFD, (struct sockaddr *)&clientAddress,
                  sizeof(clientAddress)) < 0)
        error("ERROR connecting to client");

      newSocketFD = accept(clientDataSockFD, (struct sockaddr *)&clientAddress,
                           &sizeOfClientInfo); // Accept
      if (newSocketFD < 0)
        error("ERROR on accept");

      sendDirectory(newSocketFD, directory);

      fprintf(stdout, "Command is %d\n", command);
      sendDirectory(newSocketFD, directory);
      if (command == 1) {

        /*
          int numFiles;
          int i;
          numFiles = getDirectory(directory);
          fprintf(stdout, "Directory contents:\n");
          // sendDirectory(clientSocketFD, directory);
          */
      }

      // Close the socket
      close(clientDataSockFD);
      fprintf(stdout, "Closing the connection.\n");
      close(socketFD); // Close the existing socket which is connected to the
                       // client
    }
  }

  close(listenSocketFD); // Close the listening socket

  return 0;
}
